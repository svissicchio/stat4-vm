This repository contains the virtual disk of the Virtual Machine used by Stat4, a P4 library for online statistical analyses in P4.
See https://gitlab.com/svissicchio/stat4 for additional information on Stat4!
